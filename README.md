# Backgammon Medium Gym

# Table of Contents
1. [Quick Start](#quick-start)
2. [Introduction](#introduction)
3. [Game Engine](#game-engine)
4. [Feature Space](#feature-space)
5. [Configuring Your Agent](#configuring-your-agent)
6. [Technology Used](#technology-used)

## Quick Start
To run the project make sure to install the related python packages with pip. Then run the code. This should be as simple as...

```
pip install -r requirements.txt
python3 main.py
```

## Introduction
Backgammon Medium Gym is a reinforcement learning environment modeled after Teseuvos' TD-gammon game engine. It represents the game of backgammon with a unique twist, focusing on the bearoff phase but with 12 markers instead of the traditional 6. The board state is stored as an array, manipulated according to backgammon rules. This variant includes a randomized starting position for the 15 checkers on each side, with the objective being to bear off all checkers first.

## Game Engine
The game engine manages the board state, checker movements, and winner determination. It outputs legal moves as board states and identifies the winning player. The environment's goal is to train two RL agents to select optimal moves from possible options, based on the initial board state for either black or white players.

## Feature Space
The board state comprises 28 elements:
- **White Player's Board Configuration (Elements 0-11)**: Indicates the positions of WHITE player's checkers during bearoff, with index 0 being the furthest from bearing off and 11 the closest.
- **Black Player's Board Configuration (Elements 12-23)**: Mirrors the structure of the WHITE player's board configuration.
- **Checkers Off the Board (Elements 24-25)**: Element 24 for WHITE and 25 for BLACK, showing the number of checkers each player has borne off.
- **Current Player's Turn (Elements 26-27)**: Element 26 indicates WHITE's turn and 27 BLACK's turn.

## Configuring Your Agent

### Example: Two Random Agents
`main.py` provides examples of two random agents to demonstrate the gameplay and integration of reinforcement learning agents for training or competition. The focus here is on self-play.

### Board Initialization and Legal Moves
- Initialize the board using the constructor.
- Access the `moves` attribute to iterate through legal moves.

### Agent's Responsibilities
- Supply a scalar value between 0 and 1 for each move via the `score_move` function, reflecting the move's suitability for their respective player.
- After each turn, use `get_winner` to check if the player has won.

## Technology Used
- **Python**: The primary language used for developing the game engine and scripting the reinforcement learning environment.

**Note**: This RL environment is designed for experimental and educational purposes in the realm of reinforcement learning and game strategy.
